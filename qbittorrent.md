<div align="center">
![image](/uploads/e247876072f78758c1380dc8a475350e/image.png)
</div>

# Contents
-   [Downloads](#downloads)
-   [Configuration](#configuration)
    -   [Make downloads faster](#make-downloads-faster)
    -   [Install a different qBittorrent theme](#install-a-different-qbittorrent-theme)
-   [Why is this important?](#why-is-this-important)
-   [How to add torrent files to qBittorrent](#how-to-add-torrent-files-to-qbittorrent)
-   [How to add magnet links to qBittorrent](#how-to-add-magnet-links-to-qbittorrent)
-   [Security tips and important things to know while torrenting](#security-tips-and-important-things-to-know-while-torrenting)
-   [Tracker for additional speed](#Add-trackers-manually-for-additional-download-speed)
-   [Bonus Tips and Tricks](#bonus-tips-and-tricks)

## Downloads

Windows/Mac: <https://www.qbittorrent.org/>

Linux: <https://flathub.org/apps/details/org.qbittorrent.qBittorrent> (make sure Flatpak is installed)

## Configuration

### Make downloads faster

The default configuration should be fine for most people but these are some major tweaks that you should do/ensure to achieve almost maximum speed.

Head over to the "Connection" section and ensure these settings

![config](/uploads/f37abfc6f7fb368b1977255b55fd9e9f/1.jpeg)

**Note: You don't need to change the Listening Port as it is randomized**

Also, in the BitTorrent section, ensure DHT, PeX and Local Discovery is turned on

![image](/uploads/4349ec44b98708e96e42e1b5c6e4640f/image.png)

 ### Install a different qBittorrent theme

The default user interface of qBittorrent is good but it can be improved.

There are many themes to choose from but I'll be using `darkstylesheet` theme in this example, you can find a good collection of dark themes in this GitHub repository: <https://github.com/jagannatharjun/qbt-theme>

Clone the repository and extract it. Now move the required theme to the installation folder of qBittorrent (this is optional I recommend doing this to keep your folders organized and clean)

Open qBittorrent application and go to Preferences from the Tools tab on the header bar, or simply press `Alt+O` to open it.

Click on "Use Custom UI Theme", click on the folder button and navigate to the location where your theme is located and select it.

Click on Apply/OK and restart your qBittorrent client.

![image](/uploads/1d5bb96ff217d58488261f461e5eba96/image.png)

## Why is this important?

Torrenting itself is not illegal, downloading of copyrighted content is illegal, when you are downloading using torrents, then your IP address is available to the public through the "Peers tab". What really happens when you get a C&D (Cease and Desist) letter from your ISP (Internet Service Provider) is anti-piracy companies have their employees go through the list of IP Addresses connected to a torrent swarm and send a request to their respective ISPs for shutting them down. The ISPs, in most cases simply issue the torrenters a C&D letter and that's how it ends up in your mailbox. The first 2 or 3 letters are just warnings but they might terminate your internet connection if you keep downloading content even after getting multiple warnings.

However, this can be easily circumvented by using a VPN, whether it is free or paid doesn't really matter when used for the sole purpose of torrenting. Just make sure it's not from some shady VPN company with Chinese associations.

I personally recommend **Mullvad VPN** since it's simply the best. However if you can't afford to pay for a VPN then try out **Windscribe** and **Proton VPN**. They both provide a really nice free trial.

## How to add torrent files to qBittorrent

- Download the torrent file
- Open qBittorrent application and go to File from the menu bar, then click on "Add Torrent file", or simply press `Ctrl + O` to open it.
- Navigate to the torrent file and start downloading

## How to add magnet links to qBittorrent

- Copy the magnet link
- Open qBittorrent application and go to File from the menu bar, then click on "Add Torrent link", or simply press `Ctrl + Shift + O` to open it.
- Paste the magnet link and click Download.
 
## Security tips and important things to know while torrenting

Ideally, everyone should go look up references and study how the BitTorrent protocol works, please know this is a P2P network, you are directly connected to another person who has the file you are looking for, no middlemen (servers) are involved.

- **IMPORTANT FOR VPN USERS**: Please enable Killswitch in your VPN client, what this does is terminate the VPN connection immediately if your internet connection fails, this will ensure that you never are connected to the torrent swarm without a VPN.

- You might want to bind your qBittorrent to a VPN, here's a guide on how to do so in NordVPN but the procedure should be almost the same for any other VPN provider: <https://support.nordvpn.com/Connectivity/Proxy/1087802472/Proxy-setup-on-qBittorrent.htm>

- qBittorrent uses uPnP to open and close ports on your router to find peers, this can be considered a security risk depending on your threat level however it should be fine in most cases, make sure your router has a good password and not "Username: admin, Password: admin", you can open the router settings by going to the address `192.168.0.1` or `192.168.1.1` 

- Go to <https://canyouseeme.org/> and ensure you don't have any ports open, ignore any manually opened port

- Port Forwarding can provide better speeds in some cases, especially in torrents with a low amount of seeders. This isn't really required in most cases.

- Adding torrent trackers can improve speeds, but please note that some trackers can be malicious, it is advised to stick to the trackers provided by the torrent itself.

## Bonus Trips and Tricks

- If you're downloading a movie, TV show or anime then you can enable "Download first and last pieces first" in the "Torrents option" dialog box, this will allow you to watch the video while it's downloading

- There's a voucher for Windscribe that you can use to get 30GB of free bandwidth<!--  -->

```
Windscribe 30GB Voucher
Free Until : Unknown
Key: СВОБОДА
https://windscribe.com/signup
```

## Add trackers manually for additional download speed

Sometimes trackers that come by default with the torrent you're trying to download are not providing extra uploaders (seeds), because more seeds = increase in download speed, so to fix this visit this GitHub repository: <https://github.com/ngosang/trackerslist>

- 1) Choose one of the links by clicking on **link**, this will get you into a RAW page of freshly updated trackers, select all and copy.

<div align="center">
![links](https://i.postimg.cc/YqvkZHZf/links.png)
</div>

- 2) Now in qBittorrent highlight your torrent and switch to the trackers tab, then click on the empty space and add new trackers, finally paste tracker.

<div align="center">
![guide](https://i.postimg.cc/Jztft5MN/Annotation-2022-08-12-024929.png)
</div>

- 3) At this point you're done, sometimes qBittorrent does not contact trackers which is not usually recommended, to enable this feature, in qBittorrent click on the above ribbon **Tools -> Advanced -> scroll down until you see "always annouce to all trackers in a tier" and enable it.**

- 4) And that's it, the original github repository gets refresh in a daily basis to provide all active trackers, this has been proven by many users on the pirate community that it increase your download speed.

- **NOTE : this won't work on torrents downloaded from private trackers, this method becomes useless unless using it on public torrents from public trackers.**

